﻿namespace LV6___analiza_prava
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TB1 = new System.Windows.Forms.TextBox();
            this.TB2 = new System.Windows.Forms.TextBox();
            this.CB1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CB2 = new System.Windows.Forms.ComboBox();
            this.button = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.TBRES = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Prvi operand:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Drugi operand:";
            // 
            // TB1
            // 
            this.TB1.Location = new System.Drawing.Point(143, 81);
            this.TB1.Name = "TB1";
            this.TB1.Size = new System.Drawing.Size(100, 20);
            this.TB1.TabIndex = 2;
            this.TB1.Text = "0";
            // 
            // TB2
            // 
            this.TB2.Location = new System.Drawing.Point(143, 116);
            this.TB2.Name = "TB2";
            this.TB2.Size = new System.Drawing.Size(100, 20);
            this.TB2.TabIndex = 3;
            this.TB2.Text = "0";
            // 
            // CB1
            // 
            this.CB1.FormattingEnabled = true;
            this.CB1.Items.AddRange(new object[] {
            "Zbrajanje",
            "Oduzimanje",
            "Mnozenje",
            "Dijeljenje"});
            this.CB1.Location = new System.Drawing.Point(12, 205);
            this.CB1.Name = "CB1";
            this.CB1.Size = new System.Drawing.Size(121, 21);
            this.CB1.TabIndex = 5;
            this.CB1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Osnovne operacije:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(126, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "KALKULATOR";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(186, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Napredne operacije:";
            // 
            // CB2
            // 
            this.CB2.FormattingEnabled = true;
            this.CB2.Items.AddRange(new object[] {
            "Kvadriranje",
            "Korjenovanje",
            "Logaritmiranje",
            "Sinus",
            "Kosinus"});
            this.CB2.Location = new System.Drawing.Point(178, 205);
            this.CB2.Name = "CB2";
            this.CB2.Size = new System.Drawing.Size(121, 21);
            this.CB2.TabIndex = 9;
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(12, 267);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(121, 23);
            this.button.TabIndex = 10;
            this.button.Text = "osnovne operacije";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 332);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "REZULTAT:";
            // 
            // TBRES
            // 
            this.TBRES.Enabled = false;
            this.TBRES.Location = new System.Drawing.Point(143, 332);
            this.TBRES.Name = "TBRES";
            this.TBRES.Size = new System.Drawing.Size(100, 20);
            this.TBRES.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 241);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(235, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "napredne operacije se unasaju pod prvi operand";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(178, 266);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "napredne operacije";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 409);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TBRES);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button);
            this.Controls.Add(this.CB2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CB1);
            this.Controls.Add(this.TB2);
            this.Controls.Add(this.TB1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB1;
        private System.Windows.Forms.TextBox TB2;
        private System.Windows.Forms.ComboBox CB1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CB2;
        private System.Windows.Forms.Button button;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TBRES;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
    }
}


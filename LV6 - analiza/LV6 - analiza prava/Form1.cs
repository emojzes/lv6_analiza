﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6___analiza_prava
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        double res = 0;
        private void button_Click(object sender, EventArgs e)
        {
            double op1, op2;
            res = 0;
            op1 = Convert.ToDouble(TB1.Text);
            op2 = Convert.ToDouble(TB2.Text);
            if (CB1.Text == "Zbrajanje")
                res = op1 + op2;
            else if (CB1.Text == "Oduzimanje")
                res = op1 - op2;
            else if (CB1.Text == "Mnozenje")
                res = op1 * op2;
            else if (CB1.Text == "Dijeljenje")
            {
                if (op2 == 0)
                    MessageBox.Show("Djeljenje s nulom nije dozvoljeno!", "Error");
                else
                    res = op1 / op2;
            }

            TBRES.Text = res.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double op;
            op = Convert.ToDouble(TB1.Text);
            res = 0;
            if (CB2.Text == "Kvadriranje")
                res = op * op;
            else if (CB2.Text == "Korjenovanje")
                res = Math.Sqrt(op);
            else if (CB2.Text == "Logaritmiranje")
                res = Math.Log10(op);
            else if (CB2.Text == "Sinus")
                res = Math.Sin(op);
            else if (CB2.Text == "Kosinus")
                res = Math.Cos(op);


            TBRES.Text = res.ToString();
        }
    }
}
